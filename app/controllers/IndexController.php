<?php
declare(strict_types=1);

namespace Controllers;

use Core\Utility\View;

/**
 * Class IndexController
 *
 * @package Controllers
 */
class IndexController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Персональный сайт',
                'content' => [
                    'template' => 'about.php'
                ]
            ]
        );
    }
}