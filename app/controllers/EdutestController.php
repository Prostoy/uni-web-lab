<?php
declare(strict_types=1);

namespace Controllers;

use Core\DependencyInjection\Container;
use Entities\FormData\EduTestFormData;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Core\Utility\View;
use Validators\Form\EduTestFormValidator;

/**
 * Class EdutestController
 *
 * @package Controllers
 */
class EdutestController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Пройдите простой тест',
                'content' => [
                    'template' => 'edu-test.php'
                ]
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @throws \Exception
     */
    public function saveForm(Request $request): void
    {
        /* @var \Monolog\Logger $logger */
        $logger = Container::get(LoggerInterface::class);

        $data = json_decode($request->getContent(), true);
        $logger->info('Пришли данные с формы /edu-test/', [$data]);

        $validateResult = EduTestFormValidator::validate(
            new EduTestFormData(
                $data['question1'],
                $data['question2'],
                $data['question3']
            )
        );

        View::render(
            'api/json.php',
            $validateResult
        );
    }
}