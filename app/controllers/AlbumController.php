<?php
declare(strict_types=1);

namespace Controllers;

use Entities\Photo\StaticRepo;
use Core\Utility\View;

/**
 * Class AlbumController
 *
 * @package Controllers
 */
class AlbumController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Фотоальбом',
                'content' => [
                    'template' => 'album.php',
                    'data' => StaticRepo::getPhotos()
                ]
            ]
        );
    }

}