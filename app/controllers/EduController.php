<?php
declare(strict_types=1);

namespace Controllers;

use Core\Utility\View;

/**
 * Class EduController
 *
 * @package Controllers
 */
class EduController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Учеба',
                'content' => [
                    'template' => 'edu.php'
                ]
            ]
        );
    }

}