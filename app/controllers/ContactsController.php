<?php
declare(strict_types=1);

namespace Controllers;

use Core\DependencyInjection\Container;
use Entities\FormData\ContactFormData;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Core\Utility\View;
use Validators\Form\ContactFormValidator;

/**
 * Class ContactsController
 *
 * @package Controllers
 */
class ContactsController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Напишите мне',
                'content' => [
                    'template' => 'contacts.php'
                ]
            ]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @throws \Exception
     */
    public function saveForm(Request $request)
    {
        /* @var \Monolog\Logger $logger */
        $logger = Container::get(LoggerInterface::class);

        $data = json_decode($request->getContent(), true);
        $logger->info('Пришли данные с формы /contacts/', [$data]);

        $validateResult = ContactFormValidator::validate(
            new ContactFormData((string)$data['name'], (string)$data['email'], (string)$data['message'])
        );

        View::render(
            'api/json.php',
            $validateResult
        );
    }
}