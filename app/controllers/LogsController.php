<?php
declare(strict_types=1);

namespace Controllers;

use Core\Utility\View;

/**
 * Class LogsController
 *
 * @package Controllers
 */
class LogsController
{
    public function index()
    {
        $logFile = __DIR__.'/../logs/app-'.date('Y-m-d').'.log';
        $command = 'tail -n 5 '.$logFile;
        exec($command, $res);
        $res = array_reverse($res);
        $res = implode('<br><br>',$res);

        View::render(
            'layouts/default.php',
            [
                'title'   => 'Логи',
                'content' => [
                    'template' => 'logs.php',
                    'data' => [
                        'logs' => $res
                    ]
                ]
            ]
        );
    }
}