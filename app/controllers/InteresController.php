<?php
declare(strict_types=1);

namespace Controllers;

use Entities\MyInterest\StaticRepo;
use Core\Utility\View;

/**
 * Class IndexController
 *
 * @package Controllers
 */
class InteresController
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => 'Мои интересы',

                'content' => [
                    'template' => 'interests.php',
                    'data' => StaticRepo::getInterests()
                ]
            ]
        );
    }
}