<?php
declare(strict_types=1);

namespace Controllers;

use Core\Utility\View;

/**
 * Class Controller404
 *
 * @package Controllers
 */
class Controller404
{
    public function index()
    {
        View::render(
            'layouts/default.php',
            [
                'title'   => '404 - Не найдено',
                'content' => [
                    'template' => '404.php'
                ]
            ]
        );
    }
}