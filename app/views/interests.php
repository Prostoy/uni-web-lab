<?
/** @var array $data */
?>
<!-- About Section-->
<header class="masthead page-section mb-0">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase">Мои интересы</h2>
        <!-- Icon Divider-->
        <div class="divider-custom divider-dark">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row">
            <div class="col-lg-8">
                <ul>
                    <? /** @var \Entities\MyInterest\MyInterest $interest */ ?>
                    <? foreach ($data as $interest): ?>
                        <li>
                            <a href="#<?= $interest->getCode() ?>"><?= $interest->getType() ?></a>
                        </li>
                    <? endforeach; ?>
                </ul>
                <? /** @var \Entities\MyInterest\MyInterest $interest */ ?>
                <? foreach ($data as $interest): ?>
                    <p class="lead"
                       id="<?= $interest->getCode() ?>"><?= $interest->getDescription() ?></p>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</header>