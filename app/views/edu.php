<?
/** @var array $data */
?>
<!-- About Section-->
<header class="masthead page-section mb-0">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase">Учеба</h2>
        <!-- Icon Divider-->
        <div class="divider-custom divider-dark">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row">
            <div class="col-lg-8">
                <ul>
                    <li>
                        Унверситет: Севастопольский государственный университет
                    </li>
                    <li>
                        Факультет: Информационне Системы
                    </li>
                    <li>
                        Группа: ИС/Б-17-2-О
                    </li>
                </ul>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Дисциплина</th>
                        <th scope="col">Количество часов</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>ТПР</td>
                        <td>999</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Веб-технологии</td>
                        <td>300</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>МИО</td>
                        <td>56</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</header>