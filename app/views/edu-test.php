<?
/** @var array $data */
?>

<!-- Masthead-->
<header class="masthead text-center"
        id="eduTestForm">
    <div class="container">
        <!-- Contact Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Тест по дисциплине: Основы программирования и алгоритмические
                                                                                        языки </h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Contact Section Form-->
        <div class="row"
             id="edutest-form">
            <div class="col-lg-8 mx-auto">
                <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                <form v-if="!form.sent"
                      name="sentMessage"
                      novalidate="novalidate">
                    <div class="control-group">
                        <div class="form-group controls mb-0 pb-2">
                            <label>Неизменная переменная называется</label><br>
                            <select required="required"
                                    v-model="form.fields.question1">
                                <option>Постоянная Планка</option>
                                <option>Конь</option>
                                <option>Константин</option>
                                <option>Константа</option>
                            </select>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.question1}}</p>
                        </div>
                    </div>
                    <hr color="#fff" width="100%">
                    <div class="control-group">
                        <div class="form-group controls mb-0 pb-2">
                            <label>Расшифруйте аббревиатуру DIC</label>
                            <textarea v-model="form.fields.question2"
                                      class="form-control"
                                      id="message"
                                      rows="2"
                                      placeholder="DIC - Это ..."
                                      required="required"></textarea>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.question2}}</p>
                        </div>
                    </div>
                    <hr color="#fff" width="100%">
                    <div class="control-group">
                        <div class="form-group controls mb-0 pb-2">
                            <label>Hypertext markup language - это</label><br>
                            <input type="radio"
                                   v-model="form.fields.question3"
                                   value="Язык программирования HTML">
                            <label for="male">Язык программирования HTML</label><br>
                            <input type="radio"
                                   v-model="form.fields.question3"
                                   value="Язык программирования PHP">
                            <label for="female">Язык программирования PHP</label><br>
                            <input type="radio"
                                   v-model="form.fields.question3"
                                   value="Язык гиппертекстовой разметки">
                            <label for="other">Язык гиппертекстовой разметки</label>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.question3}}</p>
                        </div>
                    </div>

                    <hr color="#fff" width="100%">
                    <br/>
                    <div class="form-group">
                        <button class="btn btn-primary btn-xl"
                                id="sendMessageButton"
                                @click="sendForm();"
                                type="button">Проверить
                        </button>
                    </div>
                </form>
                <div v-if="form.sent">
                    <p>
                        Тест успешно пройден!
                    </p>
                </div>
            </div>
        </div>

        <script>
            var eduTestForm = new Vue({
                el     : '#edutest-form',
                data   : {
                    form: {
                        fields: {
                            question1: '',
                            question2: '',
                            question3: ''
                        },
                        errors: {
                            question1: null,
                            question2: null,
                            question3: null
                        },
                        sent: false
                    }
                },
                methods: {
                    noErrors: function () {
                        var noErrors = true;
                        _.forEach(this.form.errors, function(value) {
                            if(value !== null){
                                noErrors = false;
                            }
                        });

                        return noErrors;
                    },
                    sendForm: function (event) {
                        axios({
                                headers: {'X-Requested-With': 'XMLHttpRequest'},
                                url    : '/edu-test/',
                                method : 'post',
                                data   : eduTestForm.form.fields
                            }
                        ).then(function (response) {
                            eduTestForm.form.errors = response.data.errors;

                            if(eduTestForm.noErrors()){
                                eduTestForm.form.sent = true;
                            }
                        });
                    }
                },
                mounted() {

                }
            },)
        </script>
    </div>
</header>

