<?
/** @var array $data */
?>

<!-- Masthead-->
<header class="masthead text-center" id="contactForm">
    <div class="container">
        <!-- Contact Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Напишите мне</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Contact Section Form-->
        <div class="row"
             id="contacts-form">
            <div class="col-lg-8 mx-auto">
                <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                <form v-if="!form.sent"
                      name="sentMessage"
                      novalidate="novalidate">
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Имя</label><input v-model="form.fields.name"
                                                     class="form-control"
                                                     id="name"
                                                     type="text"
                                                     placeholder="Имя"
                                                     required="required"
                                                     data-validation-required-message="Пожалуйста, укажите свое имя"/>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.name}}</p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Email</label><input v-model="form.fields.email"
                                                       class="form-control"
                                                       id="email"
                                                       type="email"
                                                       placeholder="Email"
                                                       required="required"
                                                       data-validation-required-message="Пожалуйста, укажите email, куда я могу ответить"/>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.email}}</p>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls mb-0 pb-2">
                            <label>Сообщение</label><textarea v-model="form.fields.message"
                                                              class="form-control"
                                                              id="message"
                                                              rows="5"
                                                              placeholder="Сообщение"
                                                              required="required"
                                                              data-validation-required-message="Вы что-то хотите мне сказать?"></textarea>
                            <p class="help-block text-danger"></p>
                            <p class="text-danger">{{form.errors.message}}</p>
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <button class="btn btn-primary btn-xl"
                                id="sendMessageButton"
                                @click="sendForm();"
                                type="button">Отправить
                        </button>
                    </div>
                </form>
                <div v-if="form.sent">
                    <p>
                        Сообщение успешно отправлено!
                    </p>
                </div>
            </div>
        </div>

        <script>
            var contactForm = new Vue({
                el     : '#contacts-form',
                data   : {
                    form: {
                        fields: {
                            name   : null,
                            email  : null,
                            message: null
                        },
                        errors: {
                            name   : null,
                            email  : null,
                            message: null
                        },
                        sent: false
                    }
                },
                methods: {
                    noErrors: function () {
                        var noErrors = true;
                        _.forEach(this.form.errors, function(value) {
                            if(value !== null){
                                noErrors = false;
                            }
                        });

                        return noErrors;
                    },
                    sendForm: function (event) {
                        axios({
                                headers: {'X-Requested-With': 'XMLHttpRequest'},
                                url    : '/contacts/',
                                method : 'post',
                                data   : contactForm.form.fields
                            }
                        ).then(function (response) {
                            contactForm.form.errors = response.data.errors;

                            if(contactForm.noErrors()){
                                contactForm.form.sent = true;
                            }
                        });
                    }
                },
                mounted() {

                }
            },)
        </script>
    </div>
</header>

