<?
/** @var array $data */
?>
<!-- Portfolio Section-->
<section class="page-section masthead portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Фотоальбом</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row">
            <? /* @var \Entities\Photo\Photo $photo */ ?>
            <? foreach($data as $photo): ?>
                <!-- Portfolio Item 1-->
                <div class="col-md-6 col-lg-4 mb-5">
                    <div class="mx-auto">
                        <img class="img-fluid" src="<?=$photo?>" alt="<?=$photo->getDesc()?>" />
                        <p><?=$photo->getDesc()?></p>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>