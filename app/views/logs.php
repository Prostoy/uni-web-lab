<?
/** @var array $data */
?>

<!-- Masthead-->
<header class="masthead bg-primary text-white">
    <div class="container d-flex align-items-center flex-column">
        <h1 class="masthead-heading text-uppercase mb-0">Логи<br></h1>

        <p class="lead"><?= $data['logs']?></p>
    </div>
</header>
