<?
/** @var array $data */
?>

<!-- Masthead-->
<header class="masthead bg-primary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image--><img class="masthead-avatar mb-5 rounded-circle"
                                          src="/assets/img/avatar.jpg"
                                          alt=""/><!-- Masthead Heading-->
        <h1 class="masthead-heading text-uppercase mb-0">ИС/Б-17-2-О <br></h1>
        <!-- Icon Divider-->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Masthead Subheading-->
        <p class="masthead-subheading font-weight-light mb-0">Гаврилин Андрей Владимирович</p>
    </div>
</header>

<!-- About Section-->
<section class="page-section mb-0" id="about">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase">Обо мне</h2>
        <!-- Icon Divider-->
        <div class="divider-custom divider-dark">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row">
            <div class="col-lg-4 ml-auto"><p class="lead">Я занимаюсь разработкой backend части веб и мобильных приложений<br>

                                                              В процессе работы я:
                <ul>
                    <li>Разработал централизованную систему управления каталогами нескольких интернет-магазинов на битриксе.</li>
                    <li>Применял Sphinx для реализации быстрого полнотекстового поиска в связке с ZendFramework </li>
                    <li>Интегрировал RabbitMQ в приложение почтовой рассылки</li>
                    <li>Использовал методологию разработки DDD</li>
                    <li>Участвовал в разработке системы пропусков во время карантина Covid19</li>
                </ul>
                </p>
            </div>
            <div class="col-lg-4 mr-auto"><p class="lead">Свободное время провожу в поездках и путешествиях на мотоцикле</p> <img class="img-fluid mx-auto d-block" src="/assets/img/moto.jpg"></div>
        </div>
    </div>
</section>