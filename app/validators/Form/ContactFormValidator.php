<?php
declare(strict_types=1);

namespace Validators\Form;

use Entities\FormData\ContactFormData;

/**
 * Class ContactFormValidator
 *
 * @package Validators\Form
 */
abstract class ContactFormValidator
{
    /**
     * @param \Entities\FormData\ContactFormData $data
     */
    public static function validate(ContactFormData $data)
    {
        return [
            'errors' => [
                'name'    => static::validateName($data->getName()) ? null : 'Имя должно содержать более 2 символов',
                'email'   => static::validateEmail($data->getEmail()) ? null : 'Email должен содержать символ "@"',
                'message' => static::validateMessage($data->getMessage()) ? null : 'Сообщение должно содержать минимум 20 символов',
            ]
        ];
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public static function validateName(string $name)
    {
        return 2 < strlen($name);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public static function validateEmail(string $email)
    {
        return strpos($email, '@') !== false;
    }

    /**
     * @param string $message
     *
     * @return bool
     */
    public static function validateMessage(string $message)
    {
        return 20 < strlen($message);
    }
}