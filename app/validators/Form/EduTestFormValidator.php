<?php
declare(strict_types=1);

namespace Validators\Form;

use Entities\FormData\EduTestFormData;

/**
 * Class EduTestFormValidator
 *
 * @package Validators\Form
 */
abstract class EduTestFormValidator
{
    /**
     * @param \Entities\FormData\EduTestFormData $data
     */
    static function validate(EduTestFormData $data)
    {
        $errors = [
            'question1' => null,
            'question2' => null,
            'question3' => null
        ];

        $trueAnswers = static::getTrueResult();
        $answers = $data->toArray();

        foreach ($trueAnswers as $key => $trueAnswer) {
            $errors[$key] = strtolower($trueAnswer) === strtolower($answers[$key])? null : 'Ответ не верный';
        }

        return [
            'errors' => $errors
        ];
    }

    /**
     * @return array
     */
    protected static function getTrueResult()
    {
        return [
            'question1' => 'Константа',
            'question2' => 'Dependency Injection Container',
            'question3' => 'Язык гиппертекстовой разметки'
        ];
    }
}