<?php
declare(strict_types=1);

namespace Entities\Photo;

/**
 * Class StaticRepo
 *
 * @package Entities\Photo
 */
class StaticRepo
{
    /**
     * @return array
     */
    public static function getPhotos(){
      return [
          new Photo('/assets/img/portfolio/cabin.png', 'Рыбное описание фото cabin для фотоальбома'),
          new Photo('/assets/img/portfolio/cake.png', 'Рыбное описание фото cake для фотоальбома'),
          new Photo('/assets/img/portfolio/circus.png', 'Рыбное описание фото circus для фотоальбома'),
          new Photo('/assets/img/portfolio/game.png', 'Рыбное описание фото game для фотоальбома'),
          new Photo('/assets/img/portfolio/safe.png', 'Рыбное описание фото safe для фотоальбома'),
          new Photo('/assets/img/portfolio/submarine.png', 'Рыбное описание фото submarine для фотоальбома'),
      ];
    }
}