<?php
declare(strict_types=1);

namespace Entities\Photo;

/**
 * Class Photo
 *
 * @package Entities\Photo
 */
class Photo
{
    public $path;
    public $desc;

    /**
     * Photo constructor.
     *
     * @param string $path
     * @param string $desc
     */
    public function __construct(string $path, string $desc = '')
    {
        $this->path = $path;
        $this->desc = $desc;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    public function setDesc(string $desc): void
    {
        $this->desc = $desc;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->path;
    }
}