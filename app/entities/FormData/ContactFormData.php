<?php
declare(strict_types=1);

namespace Entities\FormData;


/**
 * Class ContactFormData
 *
 * @package Entities\FormData
 */
class ContactFormData
{
    protected $name;
    protected $email;
    protected $message;

    /**
     * ContactFormData constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $message
     */
    public function __construct(string $name, string $email, string $message)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

}