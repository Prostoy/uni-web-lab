<?php
declare(strict_types=1);

namespace Entities\FormData;


/**
 * Class EduTestFormData
 *
 * @package Entities\FormData
 */
class EduTestFormData
{
    protected $question1;
    protected $question2;
    protected $question3;

    /**
     * ContactFormData constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $message
     */
    public function __construct(string $question1, string $question2, string $question3)
    {
        $this->question1 = $question1;
        $this->question2 = $question2;
        $this->question3 = $question3;
    }

    /**
     * @return string
     */
    public function getQuestion1(): string
    {
        return $this->question1;
    }

    /**
     * @return string
     */
    public function getQuestion2(): string
    {
        return $this->question2;
    }

    /**
     * @return string
     */
    public function getQuestion3(): string
    {
        return $this->question3;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'question1' => $this->question1,
            'question2' => $this->question2,
            'question3' => $this->question3,
        ];
    }

}