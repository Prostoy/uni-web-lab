<?php
declare(strict_types=1);

namespace Entities\MyInterest;

/**
 * Class MyInterest
 *
 * @package Entities\MyInterest
 */
class MyInterest
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $code;

    /**
     * MyInterest constructor.
     *
     * @param string $type
     * @param string $description
     * @param string $code
     */
    public function __construct(string $type, string $description, string $code)
    {
        $this->type = $type;
        $this->description = $description;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}