<?php
declare(strict_types=1);

namespace Entities\MyInterest;

/**
 * Class StaticRepo
 *
 * @package Entities\MyInterest
 */
class StaticRepo
{
    /**
     * @return array
     */
    public static function getInterests(): array
    {
        return [
            new MyInterest('Увлечения', 'Путешествия, мотоциклы, веб-разработка', 'interes'),
            new MyInterest('Фильмы', 'Король лев', 'film'),
            new MyInterest('Музыка',
                'Disturbed, Godsmack, Rise Against, Five Finger Death Punch, Skillet, Scorpions, Nirvana',
            'music'),
        ];
    }
}