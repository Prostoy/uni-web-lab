<?php
declare(strict_types=1);

namespace Core\Utility;


use Controllers\Controller404;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\PhpFileLoader;

/**
 * Class Router
 *
 * @package Utility
 */
class Router
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * Router constructor.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function route(): void
    {
        $fileLocator = new FileLocator([__DIR__ . '/../../config/local']);
        $loader = new PhpFileLoader($fileLocator);

        /** @var \Symfony\Component\Routing\RouteCollection $routes */
        $routes = $loader->load('routes.php');

        $context = new RequestContext();
        $context->fromRequest($this->request);

        $matcher = new UrlMatcher($routes, $context);
        $parameters = $matcher->matchRequest($this->request);

        if (class_exists($parameters['_controller'][0])) {
            $controller = new $parameters['_controller'][0];
        } else {
            throw new RuntimeException('Не найден контроллер ' . $parameters['_controller'][0]);
        }

        if (method_exists($controller, $parameters['_controller'][1])) {
            $action = $parameters['_controller'][1];
        } else {
            throw new RuntimeException('Не найден метод ' . $parameters['_controller'][0] . '::' .
                $parameters['_controller'][1]);
        }

        $controller->$action($this->request);
    }

    public function route404(): void
    {
        $controller = new Controller404();
        $controller->index();
    }
}