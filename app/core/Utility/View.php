<?php
declare(strict_types=1);

namespace Core\Utility;

/**
 * Class View
 *
 * @package Utility
 */
class View
{
    /**
     * @param string $layout
     * @param array  $data
     */
    public static function render(string $template, array $data = []){
        include __DIR__ . '/../../views/'.$template;
    }
}