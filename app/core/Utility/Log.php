<?php
declare(strict_types=1);

namespace Core\Utility;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\PsrLogMessageProcessor;
use Monolog\Processor\UidProcessor;
use Monolog\Processor\WebProcessor;

/**
 * Class Log
 */
class Log
{
    /**
     * Получить логгер
     *
     * @param string $code
     *
     * @return Logger
     *
     */
    final public static function getLog(string $code = null): Logger
    {
        # Формат записи даты в лог
        $dateFormat = 'Y-m-d H:i:s';

        # Формат записи в лог
        $output = "[%datetime%][%level_name%]: %message% %context% %extra%\n";

        # Форматтер строк логов
        $formatter = new LineFormatter($output, $dateFormat);

        $logger = new Logger($code);

        $level = self::getLogLevel();

        $stream = new RotatingFileHandler(self::getLogPath($code), 30, $level);
        $stream->setFormatter($formatter);
        $logger->pushHandler($stream);

        $logger
            ->pushProcessor(new IntrospectionProcessor())
            ->pushProcessor(new WebProcessor())
            ->pushProcessor(new MemoryPeakUsageProcessor())
            ->pushProcessor(new MemoryUsageProcessor())
            ->pushProcessor(new ProcessIdProcessor())
            ->pushProcessor(new PsrLogMessageProcessor())
            ->pushProcessor(new UidProcessor());

        return $logger;
    }

    /**
     * Получить уровень логирования
     *
     * @return int Идентификатор уровня логирования
     * @internal param string $code Код процесса
     *
     */
    private static function getLogLevel(): int
    {
        return Logger::DEBUG;
    }

    /**
     * Получить путь к логу
     *
     * @param string $code
     *
     * @return string Путь к логу
     */
    private static function getLogPath(string $code = null): string
    {
        /**
         * Хранение логов ведется в public для быстрого доступа при демонстрации
         */
        return null === $code
            ? __DIR__ . '/../../logs/app.log'
            : __DIR__ . '/../../logs/' . $code . '.log';
    }
}
