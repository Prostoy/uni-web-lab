<?php
declare(strict_types=1);

namespace Core\DependencyInjection;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Core\Utility\Log;

/**
 * Обёртка над конкретным Dependency Injection container'ом.
 *
 * Используется для регистрации зависимостей в контейнере
 * и в качестве service locator'а в клиенстком коде.
 */
class Container
{
    /**
     * @var self Singleton self instance.
     */
    private static $instance;

    /**
     * @var ContainerBuilder
     */
    private $container;


    /**
     * Инициализация контейнера
     */
    public static function reset(): void
    {
        $container = new ContainerBuilder();

        self::registerLoggers($container);

        $container->compile();

        self::$instance = new self($container);
    }

    /**
     * Регистрация логгеров
     *
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     *
     * @param string                                                  $loggerName
     */
    private static function registerLoggers(ContainerBuilder $container)
    {
        $container
            ->register(LoggerInterface::class)
            ->setFactory([Log::class, 'getLog'])
            ->setPublic(true);
    }

    /**
     * @param ContainerBuilder $container
     */
    private function __construct(
        ContainerBuilder $container
    ) {
        $this->container = $container;
    }

    /**
     * Получить зависимость из контейнера.
     *
     * Вместо инстанциации клиентских зависимостей прямо в клиентском коде
     * либо получения их из различных singleton'ов,необходимо получать их
     * в клиентском коде при помощи этого метода, используя контейнер
     * как service locator.
     *
     * @param string $id
     *
     * @return mixed Запрошенная зависимость.
     * @throws \Exception
     */
    public static function get(string $id)
    {
        if (!isset(self::$instance)) {
            self::reset();
        }

        return self::$instance->container->get($id);
    }
}
