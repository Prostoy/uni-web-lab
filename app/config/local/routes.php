<?php

use Controllers\ContactsController;
use Controllers\IndexController;
use Controllers\InteresController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('home/index', '/')
        ->controller([IndexController::class, 'index']);

    $routes->add('home/interests', '/interests/')
        ->controller([InteresController::class, 'index']);

    $routes->add('home/logs', '/logs/')
        ->controller([\Controllers\LogsController::class, 'index']);

    $routes->add('home/album', '/album/')
        ->controller([\Controllers\AlbumController::class, 'index']);

    $routes->add('home/edu', '/education/')
        ->controller([\Controllers\EduController::class, 'index']);

    $routes->add('home/edu-test', '/edu-test/')
        ->controller([\Controllers\EdutestController::class, 'index'])
        ->methods(['GET']);

    $routes->add('home/edu-test/saveForm', '/edu-test/')
        ->controller([\Controllers\EdutestController::class, 'saveForm'])
        ->methods(['POST']);

    $routes->add('home/contacts', '/contacts/')
        ->controller([ContactsController::class, 'index'])
        ->methods(['GET']);

    $routes->add('home/contacts/saveForm', '/contacts/')
        ->controller([ContactsController::class, 'saveForm'])
        ->methods(['POST']);
};