#!/usr/bin/env bash
echo "Обновляю uni-web-lab.andrey-gavrilin.ru до состояния ветки master"
date && hostname
cd /var/www/andrey/data/www/uni-web-lab.andrey-gavrilin.ru || exit
git reset --hard && git clean -df && git status -s && git checkout master && git reset --hard && git clean -df
git fetch && git pull --ff-only origin master && git reset --hard && git clean -df && git status -s
cd /var/www/andrey/data/www/uni-web-lab.andrey-gavrilin.ru/app && composer update  || exit
touch /var/www/andrey/data/www/uni-web-lab.andrey-gavrilin.ru/.dev
